//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef ____FILEBASENAME____H
#define ____FILEBASENAME____H

#include <iostream>
#include "___VARIABLE_cppSubclass___.h"

class ___FILEBASENAME___ : public ___VARIABLE_cppSubclass___	{
public:
	___FILEBASENAME___ ();
	~___FILEBASENAME___ ();
	
	
	
private:
	
};

#endif /* defined(____FILEBASENAME____H) */